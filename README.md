#django-rugeoip

## About

django-rugeoip is a Django application for IP address geo location using two databases:
**ipgeobase** for RU and UA  and **maxmind** geoip for the other countries.

## Settings

###Register Django app:

Add `'geo'` to `INSTALLED_APPS`.

###Download databases:

-   `cidr_optim.txt` and `cities.txt` from [ipgeobase](http://ipgeobase.ru/)

-   `GeoIP.dat` (or `GeoIPLite.dat`) and `GeoIPCity.dat`
   (or `GeoIPLiteCity.dat`) from [maxmind](http://dev.maxmind.com/)

### Specify databases path and filenames in your Django settings.py:

    GEOIP_PATH = '/path/to/your/databases/directory'

    GEOIP_COUNTRY = 'GeoIP.dat'
    GEOIP_CITY = 'GeoIPCity.dat'

    IPGEOBASE_COUNTRY = 'IPGeoBase.dat'
    IPGEOBASE_CITY = 'IPGeoBaseCity.dat'

### Convert ipgeobase .txt bases to binary:

    $./manage_admin geo_convert <custom path>

or

    $./manage_admin geo_convert

You will find produced `IPGEOBASE_CITY` and `IPGEOBASE_COUNTRY` binary files in
`<custom path>` or `GEOIP_PATH` directory.

Convert them every time you update ipgeobase .txt databases.


## Usage

    from pprint import pprint
    from geo import GeoIP

    gi = GeoIP()

    pprint(gi.city('www.google.com'))
    print gi.country_code('173.194.32.177')


## Run tests

    GEOIP_PATH=/path/to/geoip/dir ./runtests.py
