# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.management import call_command

from geo import GeoIP


class GeoIPTest(TestCase):
    def setUp(self):
        call_command('geo_convert')

        self.geo = GeoIP()

    def test_us_ip(self):
        self.assertEqual(self.geo.country_code('173.194.32.177'), 'US')
        self.assertEqual(self.geo.country_code('173.194.32.178'), 'US')
        self.assertEqual(self.geo.country_code('173.194.32.179'), 'US')

    def test_ru_ip(self):
        self.assertEqual(self.geo.country_code('37.144.7.23'), 'RU')

    def test_moscow_ip(self):
        self.assertEqual(self.geo.city('37.144.7.23')['city'], u'Москва')
